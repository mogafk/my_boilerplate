# Структура
server

├── fixtures

├── models

├── routes

└── tests


**fixtures** - директория содержит в себе данные, которые можно использовать для тестирования.

**models** - здесь лежат модели mongoose(схема, методы)

**routes** - роуты для разных точек. Каждый файл потом включается в server/server.js. Так же лежат схемы на валидацию входных данных к урлу(роуту) api_schemas.js. Валидация с помощью Joi

**tests** - директория с тестами. Формат должен быть обязательно *.spec.js, запускаются с помощью mocha, но из node. Если запустить NODE_ENV=testing node /index.js. Так же содержит answers_chemas.js и содержит некоторые схемы для ответов от апи. Используется для на интеграционных тестах, чтобы проверить ответ http апи

Внутри /server лежат *.config.json5 файлы и config.js. config.js смотрит на переменные окружения и отвечает за то, какой конфиг мы получим. Потенциально здесь можно было бы валидировать конфиги.

По пре-коммиту проходят тесты и работает линтеровщик.

# Апи
Есть три endpoint 


**/auth/**

HTTP GET /ensure_login возращает 401 или 200 http код.

HTTP POST /registration 

username: string

password: string минимальная длина 6

confirmPassword: string

[email]: string необяз.

В случае успеха возвращет 200 и создает нового пользователя

HTTP GET /logout сбрасывает авторизацию.

HTTP POST /login 

username: string

password: string

в случае успеха переадресует на корень

===========================================================

**/users/**
HTTP GET /users/get
возвращает все пользователей без пагинации отсортированных в порядке убывания их количества комментариев

===========================================================


**/comments/**
HTTP POST /comments/add
Принимает json из одного комментария. Струтура запроса:

_id: Joi.string().length( 24 ).required(),

root: Joi.string().length( 24 ).required(),

parent: Joi.string().length( 24 ),

childs: Joi.array().items( Joi.string().length( 24 ), Joi.string().empty()),

username: Joi.string().alphanum().min( 3 ).max( 16 ).required(),

message: Joi.string().required(),

HTTP PUT /comments/add

Добавляет несколько комментариев сразу. Структура запроса аналогична POST /comments/add, но только в массиве

HTTP GET /comments/read

Принимает либо один id как query параметр, либо несколько.

Возвращает комментарии или их массив.

HTTP GET /treeheight/byNode/:nodeId

:nodeId любой комментарий - возвращает его высоту.

HTTP GET /treeheight/byRoot/:rootId

:rootId id корня дерева. Возвращает высоту этого дерева


# Дополнительная информация

По поводу дерева комментариев: каждый комментарий хранит в себе ссылку на родителя, и id корня. В результате мы можем найти высоту дерева агрегирующим запросом: сначала выбираем все в дереве, потом с помощью $graphLookup строим его, потом считаем высоту каждого пути и выбираем наибольший.

mongo 3.4, node 6.10