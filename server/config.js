//hook for json5 parsing
require( 'json5/lib/require' )
const NODE_ENV = process.env.NODE_ENV || 'development'
module.exports = require( `./${NODE_ENV}.config.json5` )
