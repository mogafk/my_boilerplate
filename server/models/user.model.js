const mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    UserSchema = new Schema({
        username: { type: String, default: '' },
        email: { type: String, default: '' },
        password: { type: String, default: '' },
        commentsCount: { type: Number, default: '0' }
    })


UserSchema.statics.incCommentsCount = function( commentsByUser ) {
    var incQueries = []
    for ( let username in commentsByUser ) {
        incQueries.push( this.update({ username }, {
            $inc: {
                commentsCount: commentsByUser[ username ]
            }
        }))
    }
    return Promise.all( incQueries )
}
UserSchema.statics.findById = function( id, cb ) {
    return this.find({ _id: id }, ( err, docs ) => cb( err, docs[ 0 ]))
}
UserSchema.statics.findByUsername = function( username, cb ) {
    return this.find({ username: username }, ( err, docs ) => cb( err, docs[ 0 ]))
}

module.exports = mongoose.model( 'User', UserSchema )
