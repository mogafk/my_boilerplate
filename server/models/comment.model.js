const mongoose = require( 'mongoose' ),
    Schema = mongoose.Schema,
    CommentSchema = new Schema({
        username: { type: String, default: '' },
        message: { type: String, default: '' },
        childs: { type: Array, default: [] },
        root: Schema.Types.ObjectId,
        parent: Schema.Types.ObjectId
    })

var User = require( '../models/user.model.js' )
mongoose.Promise = require( 'bluebird' ),
Comment = undefined



CommentSchema.post( 'save', function( doc ) {
    User.update({ username: doc.username }, {
        $inc: { commentsCount: 1 }
    }).then()
})


// longest path of current tree
CommentSchema.statics.getHeight = function( rootId ) {
    return this.aggregate([
        {
            $match: { root: mongoose.mongo.ObjectId( rootId ) }
        },
        {
            $graphLookup: {
                from: 'comments',
                startWith: '$_id',
                connectFromField: 'parent',
                connectToField: '_id',
                as: 'childs_elem'
            }
        },
        {
            $unwind: '$childs_elem'
        },
        {
            $group: {
                '_id': { parent: '$parent', _id: '$_id' },
                'pathLength': { $sum: 1 }
            }
        },
        { '$sort': { 'pathLength': -1 } },
        { '$limit': 1 }
    ])
}

/*
*/
CommentSchema.statics.addNode = function( node ) {
    let leaf = new this( node ),
        getParent = node.parent ? this.findOne({ _id: node.parent }) : Promise.resolve()

    return getParent.then( function( parent ) {
        if ( parent ) {
            leaf._id = mongoose.Types.ObjectId()
            leaf.parent = parent._id
            leaf.root = ( parent && parent.root ) || leaf.id
            parent.childs.push( leaf._id )
            return Promise.all([
                leaf.save(),
                Comment.findByIdAndUpdate( parent._id,
                    { $push: { childs: leaf._id } },
                    { new: true }
                )
            ])
        } else {
            leaf.root = leaf.id
            return leaf.save()
        }
    })
}

Comment = mongoose.model( 'Comment', CommentSchema )
module.exports = Comment

