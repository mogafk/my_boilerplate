const express = require( 'express' ),
    http = require( 'http' ),
    bodyParser = require( 'body-parser' ),
    config = require( './config' ),
    mongoose = require( 'mongoose' )

var app = express(),
    authRoutes = require( './routes/auth.routes.js' ),
    commentsRoutes = require( './routes/comments.routes.js' ),
    usersRoutes = require( './routes/user.routes.js' )


mongoose.connect( config['MONGO_URL'], ( error ) => {
    if ( error ) {
        throw error
    }
})


app.use( bodyParser.urlencoded({ extended: false }))
app.use( bodyParser.json())
app.use( require( 'cookie-parser' )())
app.use( require( 'express-session' )({ secret: 'keyboard cat', resave: false, saveUninitialized: false }))


app.server = http.createServer( app )

app.use( '/auth/', authRoutes )
app.use( '/comments/', commentsRoutes )
app.use( '/users/', usersRoutes )


app.server.listen( config.serverPort, () => {
    console.log( `\nserver running on ${config.serverPort} port\n` )
})

module.exports = app
