const Promise = require( 'bluebird' ),
    Joi = require( 'joi' ),
    mongoose = require( 'mongoose' ),
    _ = require( 'lodash' ),
    commentItemSchema = require( './api_schemas.js' ).commentItemSchema,
    commentsCollectionSchema = require( './api_schemas.js' ).commentsCollectionSchema

var router = new require( 'express' ).Router(),
    Comment = require( '../models/comment.model.js' ),
    User = require( '../models/user.model.js' )

Joi.validate = Promise.promisify( Joi.validate )


// запрос должен представлять из себя
// id_parent not required
// message, user._id - required
// и создавать новый элемент в базе
router.post( '/add', ( req, res ) => {
    let { payload } = req.body
    Joi.validate( payload, commentItemSchema )
        .then( function() {
            return Comment.addNode( payload )
        })
        .then( function() {
            res.status( 200 ).end()
        })
        .catch( function( err ) {
            console.log( 'err', err )
            res.status( 500 ).end()
        })
})
// записывает новый список комментариев.
router.put( '/add', ( req, res ) => {
    let { payload } = req.body
    //тут существует проблема, что я не знаю
    //как нормально записывать индексируемые поля вроде
    //_id так чтобы сохранить ссылки от дерева(на рут, на парента, на чилдов)
    //индентификаторы идут вместе с данными и индексы уже могут дублировать
    //существующие индексы в бд

    //еще одна проблема в том, что insert не вызывает хуков на post-save
    //а значит мы не сможем увеличивать при insert количество комментариев
    //у пользователя
    Joi.validate( payload, commentsCollectionSchema )
        .then( function() {
            payload = req.body.payload.map( function( node ) {
                node._id = mongoose.mongo.ObjectId( node._id )
                node.parent = mongoose.mongo.ObjectId( node.parent )
                node.root = mongoose.mongo.ObjectId( node.root )
                node.childs = node.childs.map( function( child_id ) {
                    return mongoose.mongo.ObjectId( child_id )
                })

                return node
            })

            // return Comment.insertMany(payload);
            return Comment.collection.insert( payload )
        })
        .then( function() {
            let commentsByUser = _.countBy( payload, 'username' )
            return User.incCommentsCount( commentsByUser )
        })
        .then( function() {
            res.status( 200 ).end()
        })
        .catch( function( err ) {
            console.log( 'err', err )
            res.status( 500 ).end()
        })
})

//может быть
// Array<id> комментариев которые нужны
// id - комментарий который нужен
router.get( '/read', ( req, res ) => {
    let { id, ids } = req.query
    if ( ids === undefined && id !== undefined ) {
        //one
        Comment.findOne({ _id: id })
            .then( function( comment ) {
                res.json( comment )
            })
            .catch( function( err ) {
                console.log( 'ERROR', err )
            })
    } else if ( ids !== undefined && id === undefined ) {
        //multiple
        Comment.find({ _id: { $in: ids } })
            .then( function( comments ) {
                res.json( comments )
            })
            .catch( function( err ) {
                console.log( 'ERROR', err )
            })

    } else {
        res.status( 404 ).end()
    }
})

//должен принимать id одного из комментариев
//и возвращать высоту дерева, куда входит этот комментарий
router.get( '/treeheight/byNode/:nodeId', ( req, res ) => {
    //тут получаем сначала id рута, потом кидаем в статик метод модели
    let { nodeId } = req.params,
        rootId
    Comment.find({ _id: nodeId })
        .then( function( comment ) {
            rootId = comment[ 0 ].root
            Comment.getHeight( rootId )
                .then( function( height ) {
                    res.json( height[ 0 ])
                })
        })
})
router.get( '/treeheight/byRoot/:rootId', ( req, res ) => {
    //пускаем сразу на статик модели
    let { rootId } = req.params
    Comment.getHeight( rootId )
        .then( function( height ) {
            res.json( height[ 0 ])
        })
})


module.exports = router
