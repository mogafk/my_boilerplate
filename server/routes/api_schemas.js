const Joi = require( 'joi' ),
    commentItemSchema = Joi.object({
        _id: Joi.string().length( 24 ).required(),
        root: Joi.string().length( 24 ).required(),
        parent: Joi.string().length( 24 ),
        childs: Joi.array().items( Joi.string().length( 24 ), Joi.string().empty()),

        username: Joi.string().alphanum().min( 3 ).max( 16 ).required(),
        message: Joi.string().required(),
    }),
    commentsCollectionSchema = Joi.array().items( commentItemSchema ),
    registrationSchema = Joi.object({
        username: Joi.string().required(),
        password: Joi.string().min( 6 ).required(),
        confirmPassword: Joi.ref( 'password' )
    })

module.exports = { commentItemSchema, commentsCollectionSchema, registrationSchema }
