var passport = require( 'passport' ),
    Strategy = require( 'passport-local' ).Strategy,
    router = new require( 'express' ).Router(),
    User = require( '../models/user.model.js' )

const Promise = require( 'bluebird' ),
    Joi = require( 'joi' ),
    registrationSchema = require( './api_schemas.js' ).registrationSchema

Joi.validate = Promise.promisify( Joi.validate )

passport.use( new Strategy(
  ( username, password, cb ) => {
      User.findByUsername( username, ( err, user ) => {
          if ( err ) {
              return cb( err )
          }
          if ( !user ) {
              return cb( null, false )
          }
          if ( user.password != password ) {
              return cb( null, false )
          }
          return cb( null, user )
      })
  }))

passport.serializeUser(( user, cb ) => {
    cb( null, user.id )
})

passport.deserializeUser(( id, cb ) => {
    User.findById( id, ( err, user ) => {
        if ( err ) {
            return cb( err )
        }
        cb( null, user )
    })
})


router.use( passport.initialize())
router.use( passport.session())


router.get( '/ensure_login',
    ( req, res ) => {
        let isNotAuthenticated = !req.isAuthenticated || !req.isAuthenticated()
        if ( !isNotAuthenticated ) {
            res.status( 200 ).end()
        } else {
            // 401 Unauthorized
            res.status( 401 ).end()
        }
    })


router.post( '/registration', ( req, res ) => {
    let newUser = req.body
    Joi.validate( newUser, registrationSchema )
        .then( function() {
            return new User( newUser ).save()
        })
        .then( function() {
            res.status( 200 ).end()
        })
        .catch( function( err ) {
            if ( err.name === 'ValidationError' ) {
                res.send( 'bad request params' )
            } else {
                res.send( 'unknown error' )
            }

            res.status( 500 ).end()
        })
})

router.get( '/logout', ( req, res ) => {
    try {
        //не уверен, что тут нужен try-catch.
        //ошибок не возникало, но мне просто захотелось
        //его сюда влепить
        req.logout()
        res.status( 200 ).end()
    } catch ( e ) {
        res.status( 500 ).end()
    }
})

//TODO: посмотреть есть ли способ вызвать passport.authenticate не как
// обработчик урла без большой крови.
//Ответ: можно, но не знаю стоит ли перерделывать
//  с одной стороны какие редиреты в апи, с другой итак работает
router.route( '/login' )
    .post( passport.authenticate( 'local', {
        successRedirect: '/',
        failureRedirect: '/auth/bad_login'
    }))


module.exports = router
