var router = new require( 'express' ).Router(),
    User = require( '../models/user.model.js' )
const Promise = require( 'bluebird' ),
    Joi = require( 'joi' )

Joi.validate = Promise.promisify( Joi.validate )


//должна принимать на вход ничего? :с Или пагинация какая-то должна быть..
router.get( '/get', ( req, res ) => {
    User.find({}, [], { sort: { commentsCount: -1 } })
        .then( function( users ) {
            res.json( users )
        })
        .catch( function( err ) {
            console.log( 'err', err )
        })
})

module.exports = router
