const assert = require( 'assert' ),
    config = require( '../config' ),
    Promise = require( 'bluebird' ),
    Joi = require( 'joi' ),
    _ = require( 'lodash' ),
    User = require( '../models/user.model.js' ),
    chai = require( 'chai' ),
    expect = chai.expect,
    url = `http://${config.hostname}:${config.serverPort}`,
    agent = chai.request.agent( url ),
    userCollectionSchema = Joi.array().items( Joi.object({
        _id: Joi.string().length( 24 ).required(),
        username: Joi.string().required(),
        email: Joi.string().allow( '' ),
        password: Joi.string().allow( '' ),
        commentsCount: Joi.number().min( 0 ).required(),
        __v: Joi.number()
    }).required())

var newUser

Joi.validate = Promise.promisify( Joi.validate )
chai.use( require( 'chai-http' ))



//TODO: убрать рандом нафиг. Переделать на
//describe before создать аккаунт
//describe after удалить тестовый аккаунт
describe( 'user model', function() {
    it( 'should create new user', function( done ) {
        let password = 'something',
            username = 'somethingname'
        new User({ username, password }).save( function( err, user ) {
            newUser = user

            if ( err === null ) {
                done()
            }
        })
    })


    it( 'static method findById of User model', function( done ) {
        User.findById( newUser._id, function( err, user ) {
            if ( err === null && user ) {
                done()
            }
        })
    })


    it( 'static method findByUsername of User model', function( done ) {
        User.findByUsername( newUser.username, function( err, user ) {
            // console.log('err',err);
            if ( err === null && user ) {
                done()
            }
        })
    })
})


describe( 'users endpoint /users/', function() {
    it( 'get user by sort comment number', function( done ) {
        agent.get( '/users/get' )
            .query()
            .end( function( err, res ) {
                expect( res ).to.be.json
                assert.equal( 200, res.statusCode )

                let response = res.body || [],
                    max = _.maxBy( response, 'commentsCount' )

                assert.equal( max.commentsCount, response[ 0 ].commentsCount, 'response shoud be sort by desc commentsCount' )
                Joi.validate( response, userCollectionSchema )
                    .catch( function( err ) {
                        console.log( 'err', err )
                        if ( err.name === 'ValidationError' ) {
                            assert.fail( 0, 1, 'bad answer from server' )
                        } else {
                            assert.fail( 0, 1, 'unknown error' )
                        }
                    })
                    .finally( done )
            })
    })
})
