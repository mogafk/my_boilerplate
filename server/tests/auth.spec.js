const assert = require( 'assert' ),
    config = require( '../config' ),
    url = `http://${config.hostname}:${config.serverPort}`,
    User = require( '../models/user.model.js' ),
    chai = require( 'chai' ),
    expect = chai.expect,
    testUser = { username: '_test5235362', password: '_test5322123' }

var agent
chai.use( require( 'chai-http' ))
agent = chai.request.agent( url )


describe( 'auth', function() {
    before( function( done ) {
        new User( testUser ).save(() => {
            done()
        })
    })

    it( 'register new user', function( done ) {
        agent
            .post( '/auth/registration' )
            .send({
                password: 'incorrect',
                confirmPassword: 'incorrect',
                username: 'username_inc'
            })
            .end( function( err, res ) {
                assert.equal( 200, res.statusCode )
                done()
            })
    })

    it( 'try to valid login', function( done ) {
        agent
            .post( '/auth/login' )
            .send({ password: '_test5322123', username: '_test5235362' })
            .end( function( err, res ) {
                expect( res ).to.redirectTo( `${url}/` )
                done()
            })
    })

    it( 'ensure logged in', function( done ) {
        agent
            .get( '/auth/ensure_login' )
            .end( function( err, res ) {
                assert.equal( 200, res.statusCode )
                done()
            })
    })

    // TODO: придумать как избавиться от вложенности
    // возможно стоит посмотреть пакет https://www.npmjs.com/package/supertest-as-promised
    it( 'logout', function( done ) {
        agent
            .get( '/auth/logout' )
            .end( function() {
                agent
                    .get( '/auth/ensure_login' )
                    .end( function( err, res ) {
                        assert.equal( 401, res.statusCode )
                        done()
                    })
            })
    })

    it( 'try to invalid login', function( done ) {
        chai.request( url )
            .post( '/auth/login' )
            .send({ password: 'notregistreduser', username: 'notregistreduser' })
            .end( function( err, res ) {
                expect( res ).to.redirectTo( `${url}/auth/bad_login` )
                done()
            })
    })


    after( function( done ) {
        User.find( testUser ).remove( function( err ) {
            console.log( err )
            if ( err === null ) {
                done()
            }
        })
    })
})
