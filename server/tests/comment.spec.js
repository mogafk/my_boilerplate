const config = require( '../config' ),
    Promise = require( 'bluebird' ),
    Joi = require( 'joi' ),
    _ = require( 'lodash' ),
    url = `http://${config.hostname}:${config.serverPort}`,
    User = require( '../models/user.model.js' ),
    Comment = require( '../models/comment.model.js' ),
    commentItemSchema = require( './answers_chemas.js' ).commentItemSchema,
    commentsCollectionSchema = require( './answers_chemas.js' ).commentsCollectionSchema,
    chai = require( 'chai' ),
    expect = chai.expect,
    assert = chai.assert,
    agent = chai.request.agent( url )


var data = require( '../fixtures/comments.json' )

Joi.validate = Promise.promisify( Joi.validate )
chai.use( require( 'chai-http' ))


describe( 'test /comments/ endpoint', function() {
    before( function( done ) {
        let usernameGrouped = _.countBy( data, 'username' ),
            usernames = _.keys( usernameGrouped )
        User.remove({})
            .then( function() {
                return Promise.map( usernames, function( username ) {
                    return User.findOne({ username })
                })
            })
            .then( function() {
                return Promise.map( usernames, function( username ) {
                    return new User({ username }).save()
                })
            })
            .catch( function( err ) {
                console.log( 'ERROR', err )
            })
            .finally( done )
    })

    it( 'put /add/ (multiple insert)', function( done ) {
        //сами комментарии
        Comment.remove({}).then( function() {
            agent.put( '/comments/add' )
                .send({ payload: data })
                .end( function( req, res ) {
                    assert.equal( 200, res.statusCode )
                    done()
                })
        })
    })

    it( 'post /add/ (once insert)', function( done ) {
        agent.post( '/comments/add' )
            .send({
                payload: {
                    '_id': '58d5399a29f3ea6af023ad90',
                    'root': '58d5399a29f3ea6af023ad90',
                    'childs': [],
                    'message': 'one insert api call',
                    'username': 'username1'
                }
            })
            .end( function( req, res ) {
                assert.equal( 200, res.statusCode )
                done()
            })
    })

    it( 'get one comment by id', function( done ) {
        agent.get( '/comments/read' )
            .query({ id: '58d5399a29f3ea6af023ad90' })
            .end( function( req, res ) {
                expect( res ).to.have.status( 200 )
                expect( res ).to.be.json
                let response = res.body || {}
                Joi.validate( response, commentItemSchema )
                    .catch( function( err ) {
                        if ( err.name === 'ValidationError' ) {
                            assert.fail( 0, 1, 'bad answer from server' )
                        } else {
                            assert.fail( 0, 1, 'unknown error' )
                        }
                    })
                    .finally( done )
            })
    })

    it( 'get multiple comments by array', function( done ) {
        agent.get( '/comments/read' )
            .query({ ids: [ '58d5399a29f3ea6af023ad90' ] })
            .end( function( req, res ) {
                expect( res ).to.have.status( 200 )
                expect( res ).to.be.json
                let response = res.body || []
                Joi.validate( response, commentsCollectionSchema )
                    .catch( function( err ) {
                        if ( err.name === 'ValidationError' ) {
                            assert.fail( 0, 1, 'bad answer from server' )
                        } else {
                            assert.fail( 0, 1, 'unknown error' )
                        }
                    })
                    .finally( done )
            })
    })
})

describe( 'comment model', function() {
    it( 'add root', function( done ) {
        Comment.addNode({
            username: 'testuser',
            message: 'message of testuser',
        }).then( function() {
            done()
        })
    })

    it( 'add new root with childs', function( done ) {
        Comment.addNode({
            username: 'testuser',
            message: 'message of testuser',
        }).then( function( parentComment ) {
            //развернуть сделать return comment.addNodе(стр ка ниже)
            Comment.addNode({
                username: 'testuser2',
                message: 'something message',
                parent: parentComment._id
            }).then( function( result ) {
                let [ comment, parentComment ] = result,
                    parentChilds = parentComment.childs.map( objId => String( objId ))


                assert.equal( String( comment.parent ), String( parentComment._id ), 'link to parent is false' )
                assert.include( parentChilds, String( comment._id ), 'link to childs from parent is false' )
                assert.equal( String( comment.root ), String( parentComment.root ), 'links to root of nodes from general tree not same' )
                done()
            })
        })
    })

    it( 'hook post save', function( done ) {
        let username = 'username5',
            user
        User.findOne({ username })
            .then( function( res ) {
                user = res
                return Comment.addNode({
                    username: username,
                    message: 'messsage'
                })
            }).then( function() {
                return User.findOne({ username })
            }).then( function( res ) {
                assert.equal( user.commentsCount + 1, res.commentsCount, 'commentsCount should be inc by 1 after save new comment' )
            }).catch( function( err ) {
                console.log( err )
            }).finally( done )
    })
})

describe( 'calculate height of tree', function() {
    var comment
    //комментарий, на котором
    //будут пущены тесты на высчитывание высоты
    //через http api и метод модели
    before( function( done ) {
        Comment.find({
            username: 'username9'
        })
        .then( function( result ) {
            comment = result[ 0 ]
            done()
        })
    })

    describe( 'call method of model Comment', function() {
        it( 'pass root id of tree', function( done ) {
            Comment.getHeight( comment.root )
                .then( function( result ) {
                    assert.equal( result[ 0 ].pathLength, 5, 'bad height' )
                    done()
                })
        })
    })

    describe( 'request to /comments/ endpoint', function() {
        it( 'pass one of tree node id', function( done ) {
            agent.get( `/comments/treeheight/byNode/${comment._id}` )
                .end( function( err, res ) {
                    let { pathLength } = res.body

                    expect( res ).to.be.json
                    assert.equal( pathLength, 5, 'bad height' )
                    done()
                })
        })
        it( 'pass root id of tree', function( done ) {
            agent.get( `/comments/treeheight/byRoot/${comment.root}` )
                .end( function( err, res ) {
                    let { pathLength } = res.body

                    expect( res ).to.be.json
                    assert.equal( pathLength, 5, 'bad height' )
                    done()
                })
        })
    })
})
