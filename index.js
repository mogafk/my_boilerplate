const NODE_ENV = process.env.NODE_ENV || "development";
console.log(`NODE_ENV == ${NODE_ENV}`);

if(NODE_ENV == 'development'){
    require('./server/server.js');
}else if(NODE_ENV == 'testing'){
    var app = require('./server/server.js');

    var Mocha = require('mocha'),
    fs = require('fs'),
    path = require('path');

    var mocha = new Mocha();
    var testDir = './server/tests'

    fs.readdirSync(testDir).filter(function(file){
        return file.substr(-7) === 'spec.js';
    }).forEach(function(file){
        mocha.addFile(
            path.join(testDir, file)
        );
    });

    mocha.run(function(failures){
        app.server.close();
        process.exit();
    });
}